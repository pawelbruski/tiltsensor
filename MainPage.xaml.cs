﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Microsoft.IoT.DeviceCore.Adc;
using Microsoft.IoT.Devices.Adc;
using Microsoft.IoT.DeviceCore.Sensors;
using Microsoft.IoT.Devices.Sensors;
using Windows.Devices.Gpio;
using Windows.UI;
using System.Diagnostics;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace LightReader
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            this.Loaded += MainPage_Loaded;
        }

        private async void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            var gpioController = GpioController.GetDefault();
            var adcManager = new AdcProviderManager();
            
            adcManager.Providers.Add(
                new ADC0832()
                {
                    ChipSelectPin = gpioController.OpenPin(18),
                    ClockPin = gpioController.OpenPin(23),
                    DataPin = gpioController.OpenPin(24),
                });
            
            var adcControllers = await adcManager.GetControllersAsync();
            
            var lightSensor = new AnalogSensor()
            {
                AdcChannel = adcControllers[0].OpenChannel(0),
                ReportInterval = 250,
            };
            
            lightSensor.ReadingChanged += LightSensor_ReadingChanged;
        }

        private async void LightSensor_ReadingChanged(IAnalogSensor sender, AnalogSensorReadingChangedEventArgs args)
        {
            // Invert
            var reading = 1 - args.Reading.Ratio;

            // Update
            await Dispatcher.RunIdleAsync((s) =>
            {
                if (reading == 1)
                {
                    Debug.WriteLine("Tilt!");
                }
            });

        }
    }
}
